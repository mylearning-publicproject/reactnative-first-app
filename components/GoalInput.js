import React from "react";
import { StyleSheet, View, TextInput, Button, Modal } from "react-native";

const GoalInput = (props) => {
  return (
    <Modal visible={props.modalVisible} animationType="slide">
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="course goal"
          onChangeText={props.textInputChange}
          style={styles.input}
        />
        <View style={styles.buttonContainer}>
          <Button title="Cancel" color="red" onPress={props.isCancel} />
          <Button title="Add" onPress={props.addGoalhandler} />
        </View>
      </View>
    </Modal>
  );
};

export default GoalInput;

const styles = StyleSheet.create({
    inputContainer: {
        flex:1,
        justifyContent:"center",
        alignItems: "center",
      },
    input: {
        borderBottomColor: "black",
        borderWidth: 2,
        width: "70%",
        height: 30,
      },
    buttonContainer:{
        flexDirection: "row",
        justifyContent: "space-between"
    }
});
