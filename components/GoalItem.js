import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const GoalItem = (props) => {
  console.log("all props", props.itemValue, props.itemKey)
  return (
    <TouchableOpacity onPress={()=>props.removeGoalHandler(props.itemValue, props.itemKey)}>
    <View style={styles.items}>
      <Text> {props.itemValue}</Text>
    </View>
    </TouchableOpacity>
  );
};

export default GoalItem;

const styles = StyleSheet.create({
    items: {
        borderColor: "black",
        borderWidth: 1,
        margin: 10,
        backgroundColor: "grey",
      },
})