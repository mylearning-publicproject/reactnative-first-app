import React, { useState } from "react";
import { StyleSheet, View, SafeAreaView, FlatList, Button } from "react-native";
import GoalItem from "./components/GoalItem";
import GoalInput from "./components/GoalInput";

export default function App() {
  const [enteredGoal, setEnteredGoal] = useState("");
  const [listOfGoals, setListOfGoals] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);

  const textInputChange = (textEntered) => {
    setEnteredGoal(textEntered);
  };

  const addGoalhandler = () => {
    setListOfGoals([
      ...listOfGoals,
      { key: Math.random().toString(), value: enteredGoal },
    ]);
    setIsAddMode(false);
  };

  const removeGoalHandler = (_, itemKeys) => {
    const newList = listOfGoals.filter((item) => item.key !== itemKeys);
    setListOfGoals(newList)
  };

  isAddModalFunction = () => {
    setIsAddMode(true)
  }

  return (
    <SafeAreaView>
      <Button title="Add a new Item" onPress={isAddModalFunction} />
      <View style={styles.screen}>
        <GoalInput
          textInputChange={textInputChange}
          addGoalhandler={addGoalhandler}
          modalVisible={isAddMode}
          isCancel={()=> setIsAddMode(false)}
        />
        <View>
          <FlatList
            keyExtractor={(item) => item.key}
            data={listOfGoals}
            renderItem={(itemData) => (
              <GoalItem
                itemValue={itemData.item.value}
                itemKey={itemData.item.key}
                removeGoalHandler={removeGoalHandler}
              />
            )}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 50,
  },
});
